# `libctest`
A simple, single header file for `C` unit testing. It supports nested test
suites, test cases, and fixtures.

## Licence
`libctest` is public domain.

## Requirements
A standard POSIX system.

## Rationale
`libctest` has two main design goals: simplicity and ease of use. It only
includes features necessary for a unit testing framework, and no new features
will be added unless a compelling reason exists to add them. The entire
framework fits in a single `C` header file, less than one thousand lines of
code. Using it is very simple: copy the header file to your source tree, make a
new `.c` file to hold your tests, create `TEST_CASE`s, add them to a
`TEST_SUITE`, and finally call `TEST_MAIN` with your suite. Compile your `.c`
file to create a test program to test your library.

## Compilation and installation
No compilation is necessary, as the entire library fits in a single header
file. Just copy it to your source tree. Alternatively, you can add it as a meson
subproject/dependency. To do so, create the following `libctest.wrap` file in
your `subprojects` folder:

```wrap
[wrap-git]
url = https://gitlab.com/katewulff86/libctest.git
revision = head
```
and then in your `meson.build` add
```meson
libctest_dep = dependency('libctest', fallback: ['libctest', 'libctest_dep'])
```
`libctest_dep` can then be added to your test binary's dependencies.

You can see this project's `meson.build` and `selftest.c` as an example of how
this works.

## Usage
### Example program
A simple usage example follows. To create a test program, add your test
functions to a new `.c` file, create test cases with the `TEST_CASE` macro,
which can then be added to a test suite with the `TEST_SUITE` macro. Finally,
call the `TEST_MAIN` macro with your test suite. Compile your `.c` file to
create an executable.

```c
/* my_library.c */
int my_great_function()
{
    return 0;
}
```
```c
/* my_library.h */
int my_great_function();
```
```c
/* test_my_library.c */
#include "ctest.h"
#include "my_library.h"

void my_function_test(void)
{
    assert(my_great_function() == 0);
}

typedef struct my_fixture_type {
    int wizz;
    char bang;
} my_fixture_type;

void my_function_with_fixture_test(my_fixture_type *object)
{
    assert(my_great_function() == object->wizz); /* oops! */
}

void *my_fixture_setup_function(void *o)
{
    (void) o;
    my_fixture_type *object = malloc(sizeof(*object));
    /* setup your fixture */
    object->wizz = 7;

    return object;
}

void my_fixture_teardown_function(my_fixture_type *object)
{
    /* teardown your fixture */
    free(object);
}

test_fixture my_fixture = {
    .parent = NULL,             /* fixtures can be nested */
    .setup = (test_setup_func) my_fixture_setup_function,
    .teardown = (test_teardown_func) my_fixture_teardown_function
};

TEST_CASE(my_function_test)
TEST_CASE_WITH_FIXTURE(my_function_with_fixture_test, &my_fixture)
TEST_SUITE(my_test_suite,
           &my_function_test_case,
           &my_function_with_fixture_test_case)
TEST_MAIN(&my_test_suite)
```

A real world example can be found in `selftest.c`, which is used to test
`ctest` with itself.

### `libctest` macros
Test cases are created with the `TEST_CASE` macro. It takes one
argument, which is the function to be called. Alternatively, a test case with a
fixture can be created with `TEST_CASE_WITH_FIXTURE`, which takes the function
and a test fixture object. These macro automatically create an object
which has the same name as the function with `_case` appended to the end. So,
`TEST_CASE(function_test)` will create an object with the name
`function_test_case`.

These test cases can then be added to a test suite with the `TEST_SUITE` macro.
Its first argument is the name of the test suite object to be created, and then
a variable number of arguments, which can be either test cases or other test
suites.

The `TEST_MAIN` macro takes one required argument, which can be either a test
case or a test suite. `TEST_MAIN` creates a standard `C` `main` function. With
this, we can compile the `test_my_library.c` file to create a program which
tests `my_library`.

### The generated test program
By default, this program behaves
like a normal UNIX program, meaning that if no test results in an error, the
program will print nothing and return 0. If a test case/suite does fail, it
prints a diagnostic, and returns the total number of errors. 

If you would like more verbose output, you can pass `-v` on the command line.
Alternatively, machine readable `JSON` output can produced by passing `-f json`.
Note that if this option is supplied, `-v` will be ignored.

By default all test suites and cases will be run. In order to only run some
tests, pass their names as arguments on the command line after the options.
For example,
```
$ ./test_my_library -v my_function_test
```
would only run the `my_function_test` test case.

The arguments
will be interpreted as regular expressions, so `simple_.*_test_case` will match
any test case name of that form. The names of the test suites to which test case
belongs will be prepended to it (with slashes in between), so if the test case
`amazing_test_case` is a part of the test suite `great_suite`, the regular
expression will be matched against `greate_suite/amazing_test_case`. This means
that an entire test suite can be matched with the simple regular expression
`greate_suite`.

Multiple regular expressions can be provided on the command line, and a test
case will be run if it matches any of them. Assuming all test cases have unique
names with no spaces, you can run select ones just by naming them.

### Further details

Test cases are run in separate processes, and will capture standard error
signals such as `SIGABRT` and `SIGSEGV`. These are used to report failures.
`assert` failures are also captured.

While your `.c` file can be compiled manually like any other `C` program,
`ctest` does integrate well with meson, as shown in the following example:

```meson
my_library = library(
    'my_library',
    'my_library.c',
)

my_library_dep = declare_dependency(
    link_with: my_library,
)

test_my_library = executable(
    'test_my_library',
    'test_my_library.c',
    dependencies: [libctest_dep, my_library_dep],
)

test(
    'test_my_library',
    test_my_library,
    args: '-v'
)
```

A real world example can be found in this project's `meson.build` file. 

## Features
`libctest` is intentionally very simple, but it does have the essentials.

Any number of test cases can be added to a test suite, and test suites can be 
nested in test suites to an arbitrary depth. Fixtures can be used to
setup/teardown objects for testing. Fixtures can be chained, and parent fixtures
will be setup before child fixtures. Fixture setup functions are passed the
object to be constructed, and are expected to return it. Top level setup
functions will need to allocate it.

Test cases are run sequentially, each in its own separate process. Any output
printed to `stdout` or `stderr` is captured, and will be displayed in the final
report (if appropriate).

The report functions print out successes/errors, as well as long how long each
test case took. A final report will display the total number of
successes/errors. Producing `JSON` output is also supported.

### Future features
Testing in parallel with multiple threads is planned.

