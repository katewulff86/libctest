/* SPDX-License-Identifier: Unlicense */

#ifndef CTEST_H
#define CTEST_H

#include <sys/wait.h>
#include <assert.h>
#include <regex.h>
#include <signal.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#define TEST_CASE(func_) \
test_case func_##_case = { \
    .test = { \
        .functions    = &test_case_functions, \
        .name         = #func_, \
    }, \
    .fixture      = NULL, \
    .func         = (test_no_env_func) func_, \
    .object       = NULL, \
};

#define TEST_CASE_WITH_FIXTURE(func_, fixture_) \
test_case func_##_case = { \
    .test = { \
        .functions    = &test_case_functions, \
        .name         = #func_, \
    }, \
    .fixture      = fixture_, \
    .func         = (test_func) func_, \
    .object       = NULL, \
};

#define TEST_SUITE(name_, ...) \
void *__##name_##_tests[] = { \
    __VA_ARGS__, \
}; \
test_suite name_ = { \
    .test         = { \
        .functions    = &test_suite_functions, \
        .name         = #name_, \
    }, \
    .tests        = (test **) __##name_##_tests, \
    .length       = sizeof(__##name_##_tests)/sizeof(__##name_##_tests[0]), \
};

#define TEST_MAIN(suite) \
int \
main(int argc, char *argv[]) \
{ \
    return test_default_main(argc, argv, (test *) suite); \
}

typedef struct test test;
typedef struct test_result test_result;
typedef struct test_fixture test_fixture;
typedef struct test_case test_case;
typedef struct test_suite test_suite;
typedef struct test_case_result test_case_result;
typedef struct test_suite_result test_suite_result;
typedef struct test_name_hierarchy test_name_hierarchy;
typedef struct test_report_settings test_report_settings;
typedef struct test_settings test_settings;

typedef test_result *(*test_run_func)(test *, test_name_hierarchy const *,
                                      test_settings const *);
typedef int (*test_report_default_func)(test_result const *,
                                        test_name_hierarchy const *);
typedef int (*test_report_verbose_func)(test_result const *,
                                        test_name_hierarchy const *);
typedef int (*test_report_json_func)(test_result const *);
typedef void *(*test_setup_func)(void *);
typedef void (*test_teardown_func)(void *);
typedef void (*test_func)(void *);
typedef void (*test_no_env_func)();
typedef void *(*test_thunk_func)(void *);

typedef struct test_functions {
    test_run_func   run;
} test_functions;

struct test {
    test_functions const *functions;
    char const     *name;
};

typedef struct test_result_functions {
    test_report_default_func report_default;
    test_report_verbose_func report_verbose;
    test_report_json_func report_json;
} test_result_functions;

struct test_result {
    test_result_functions const *functions;
    double          time;
};

struct test_fixture {
    test_fixture   *parent;
    test_setup_func setup;
    test_teardown_func teardown;
};

struct test_case {
    test            test;
    test_fixture   *fixture;
    test_func       func;
    void           *object;
};

struct test_suite {
    test            test;
    test          **tests;
    size_t          length;
};

struct test_case_result {
    test_result     result;
    test_case      *test_case_;
    char const     *out_message;
    char const     *err_message;
    bool            pass;
};

struct test_suite_result {
    test_result     result;
    test_suite     *test_suite_;
    test_result   **results;
    size_t          length;
};

struct test_name_hierarchy {
    test_name_hierarchy const *parent;
    char const      *name;
};

struct test_settings {
    bool verbose;
    bool json;
    regex_t *regex;
    size_t regex_length;
};

static int      test_default_main(int, char *[], test *);
static test_settings test_parse_command_line(int, char *[]);

static test_result *test_run(test *, test_name_hierarchy const *,
                             test_settings const *);
static test_suite_result *test_suite_run(test_suite *,
                                         test_name_hierarchy const *,
                                         test_settings const *);
static test_suite_result *test_suite_result_make(test_suite *);
static test_case_result *test_case_run(test_case *, test_name_hierarchy const *,
                                       test_settings const *);
static bool     test_requested(char const *, test_settings const *);
static test_case_result *test_run_test_case_in_new_process(test_case *);
static void     test_monitor_test_case(test_case *, int, int);
static void     test_setup_signal_handlers();
static void     test_signal_handler(int);
static void    *test_case_setup(test_case *);
static void     test_case_teardown(test_case *);
static void     test_global_test_case_teardown();
static void    *test_fixture_setup(test_fixture const *);
static void     test_fixture_teardown(test_fixture const *, void *);
static test_case_result *test_retrieve_test_case_results(test_case *, pid_t,
                                                         int, int);
static test_case_result *test_case_result_make(test_case *);
static char    *test_case_capture_output(int);
static char    *test_read_all_from_file(FILE *);

static int      test_result_report_default(test_result *,
                                           test_name_hierarchy const *);
static int      test_result_report_verbose(test_result *,
                                           test_name_hierarchy const *);
static int      test_result_report_json(test_result *);
static int      test_suite_result_report_default(test_suite_result *,
                                                 test_name_hierarchy const *);
static int      test_suite_result_report_verbose(test_suite_result *,
                                                 test_name_hierarchy const *);
static int      test_suite_result_report_json(test_suite_result *);
static int      test_case_result_report_default(test_case_result *,
                                                test_name_hierarchy const *);
static int      test_case_result_report_verbose(test_case_result *,
                                                test_name_hierarchy const *);
static int      test_case_result_report_json(test_case_result *);
static void     test_name_hierarchy_print(test_name_hierarchy const *);

static double   test_time_function(test_thunk_func, void *, void **);
static char    *test_string_escape(char const *);
static char    *test_construct_test_name(test_name_hierarchy const *);

static test_case *test_global_test_case;
static int      test_global_out_fd;
static int      test_global_err_fd;

static const test_functions test_case_functions = {
    .run = (test_run_func) test_case_run,
};

static const test_result_functions test_case_result_functions = {
    .report_default =
        (test_report_default_func) test_case_result_report_default,
    .report_verbose =
        (test_report_verbose_func) test_case_result_report_verbose,
    .report_json =
        (test_report_json_func) test_case_result_report_json,
};

static const test_functions test_suite_functions = {
    .run = (test_run_func) test_suite_run,
};

static const test_result_functions test_suite_result_functions = {
    .report_default =
        (test_report_default_func) test_suite_result_report_default,
    .report_verbose =
        (test_report_verbose_func) test_suite_result_report_verbose,
    .report_json =
        (test_report_json_func) test_suite_result_report_json,
};


inline int
test_default_main(int argc, char *argv[], test *test_object)
{
    test_settings settings = test_parse_command_line(argc, argv);
    test_result *result = test_run(test_object, NULL, &settings);
    int err;
    if (settings.json) {
        err = test_result_report_json(result);
        puts("");
    } else if (settings.verbose) {
        err = test_result_report_verbose(result, NULL);
    } else {
        err = test_result_report_default(result, NULL);
    }

    return err;
}

test_settings
test_parse_command_line(int argc, char *argv[])
{
    test_settings settings = {
        .verbose = false,
        .json = false,
    };

    int c;

    while ((c = getopt(argc, argv, ":vf:")) != -1) {
        switch (c) {
        case 'v':
            settings.verbose = true;
            break;
        case 'f':
            if (strcmp(optarg, "json") != 0) {
                fprintf(stderr, "format option %s not supported\n", optarg);
                exit(EXIT_FAILURE);
            }
            settings.json = true;
            break;
        case '?':
            fprintf(stderr, "unsupported option: '-%c'\n", optopt);
            exit(EXIT_FAILURE);
            break;
        case ':':
            fprintf(stderr, "option -%c requires operand\n", optopt);
            exit(EXIT_FAILURE);
            break;
        }
    }

    settings.regex_length = argc - optind;
    settings.regex = calloc(sizeof(*settings.regex), settings.regex_length);
    for (int i = optind; i < argc; ++i) {
        if (regcomp(&settings.regex[i-optind], argv[i],
                    REG_EXTENDED | REG_NOSUB)) {
            fprintf(stderr, "could not compile regular expression "
                    "'%s'\n", argv[i]);
            exit(EXIT_FAILURE);
        }
    }

    return settings;
}

test_result *
test_run(test *test, test_name_hierarchy const *hierarchy,
         test_settings const *settings)
{
    return test->functions->run(test, hierarchy, settings);
}

test_suite_result *
test_suite_run(test_suite *test_suite, test_name_hierarchy const *parent,
               test_settings const *settings)
{
    test_name_hierarchy hierarchy = {
        .parent       = parent,
        .name         = test_suite->test.name,
    };
    test_suite_result *result = test_suite_result_make(test_suite);
    double time = 0.0;
    size_t index = 0;

    for (size_t i = 0; i < test_suite->length; ++i) {
        test *test = test_suite->tests[i];
        test_result *test_result = test_run(test, &hierarchy, settings);
        if (test_result != NULL) {
            result->results[index++] = test_result;
            time += test_result->time;
        }
    }
    result->length = index;
    result->result.time = time;

    return result;
}

test_suite_result *
test_suite_result_make(test_suite *test_suite_)
{
    test_suite_result *result = calloc(sizeof(*result), 1);
    result->result = (test_result) {
        .functions    = &test_suite_result_functions,
    };

    result->test_suite_ = test_suite_;
    result->length = test_suite_->length;
    result->results = calloc(sizeof(*result->results), result->length);

    return result;
}

test_case_result *
test_case_run(test_case *test_case, test_name_hierarchy const *parent,
              test_settings const *settings)
{
    test_name_hierarchy hierarchy = {
        .parent       = parent,
        .name         = test_case->test.name,
    };
    test_case_result *result = NULL;
    char *name = test_construct_test_name(&hierarchy);
    if (test_requested(name, settings)) {
        double time = test_time_function((test_thunk_func)
                                          test_run_test_case_in_new_process,
                                          test_case, (void **) &result);
        result->result.time = time;
    }
    free(name);

    return result;
}

bool
test_requested(char const *name, test_settings const *settings)
{
    if (settings->regex_length == 0)
        return true;

    for (size_t i = 0; i < settings->regex_length; ++i) {
        if (regexec(&settings->regex[i], name, 0, NULL, 0) != REG_NOMATCH)
            return true;
    }

    return false;
}

test_case_result *
test_run_test_case_in_new_process(test_case *test_case)
{
    pid_t pid;
    int out_fildes[2];
    int err_fildes[2];
    pipe(out_fildes);
    pipe(err_fildes);
    if ((pid = fork()) == 0) {
        close(out_fildes[0]);
        close(err_fildes[0]);
        test_monitor_test_case(test_case, out_fildes[1], err_fildes[1]);

        return NULL;
    } else {
        close(out_fildes[1]);
        close(err_fildes[1]);

        return test_retrieve_test_case_results(test_case, pid, out_fildes[0],
                                               err_fildes[0]);
    }
}

void
test_monitor_test_case(test_case *test_case, int out_fd, int err_fd)
{
    test_global_test_case = test_case;
    test_global_out_fd = out_fd;
    test_global_err_fd = err_fd;
    test_setup_signal_handlers();
    dup2(out_fd, 1);
    dup2(err_fd, 2);
    atexit(test_global_test_case_teardown);
    if (test_case->fixture == NULL) {
        test_no_env_func func = (test_no_env_func) test_case->func;
        func();
    } else {
        test_case->object = test_case_setup(test_case);
        test_func func = test_case->func;
        func(test_case->object);
    }

    exit(0);
}

void
test_setup_signal_handlers()
{
    signal(SIGABRT, test_signal_handler);
    signal(SIGTERM, test_signal_handler);
    signal(SIGSEGV, test_signal_handler);
    signal(SIGILL, test_signal_handler);
    signal(SIGFPE, test_signal_handler);
}

void
test_signal_handler(int signal)
{
    char const *signal_name = NULL;
    switch(signal) {
    case SIGABRT:
        signal_name = "SIGABRT";
        break;
    case SIGTERM:
        signal_name = "SIGTERM";
        break;
    case SIGSEGV:
        signal_name = "SIGSEGV";
        break;
    case SIGILL:
        signal_name = "SIGILL";
        break;
    case SIGFPE:
        signal_name = "SIGFPE";
        break;
    default:
        signal_name = "unknown signal";
        break;
    }
    fprintf(stderr, "Program received %s (%i).\n", signal_name, signal);
    exit(EXIT_FAILURE);
}

void *
test_case_setup(test_case *test_case)
{
    test_fixture *fixture = test_case->fixture;
    return test_fixture_setup(fixture);
}

void
test_case_teardown(test_case *test_case)
{
    test_fixture *fixture = test_case->fixture;
    void *object = test_case->object;
    test_fixture_teardown(fixture, object);
}

void
test_global_test_case_teardown()
{
    close(test_global_out_fd);
    close(test_global_err_fd);
    test_case_teardown(test_global_test_case);
}

void *
test_fixture_setup(test_fixture const *fixture)
{
    if (fixture == NULL) {
        return NULL;
    }

    test_fixture *parent = fixture->parent;
    void *object = test_fixture_setup(parent);
    test_setup_func setup = fixture->setup;
    if (setup != NULL) {
        object = setup(object);
    }

    return object;
}

void
test_fixture_teardown(test_fixture const *fixture, void *object)
{
    if (fixture != NULL) {
        test_teardown_func teardown = fixture->teardown;
        if (teardown != NULL) {
            teardown(object);
        }
        test_fixture *parent = fixture->parent;
        test_fixture_teardown(parent, object);
    }
}

test_case_result *
test_retrieve_test_case_results(test_case *test_case, pid_t pid, int out_fd,
                                int err_fd)
{
    int stat;
    char *out_message = test_case_capture_output(out_fd);
    char *err_message = test_case_capture_output(err_fd);
    waitpid(pid, &stat, 0);
    test_case_result *result = test_case_result_make(test_case);
    result->out_message = out_message;
    result->err_message = err_message;
    if (WIFEXITED(stat))
        result->pass = WEXITSTATUS(stat) == 0;
    else
        result->pass = false;
    close(out_fd);
    close(err_fd);

    return result;
}

test_case_result *
test_case_result_make(test_case *test_case)
{
    test_case_result *result = calloc(1, sizeof(*result));
    result->result = (test_result) {
        .functions    = &test_case_result_functions,
    };
    result->test_case_ = test_case;
    return result;
}

char *
test_case_capture_output(int fd)
{
    FILE *file = fdopen(fd, "r");
    char *result = test_read_all_from_file(file);
    fclose(file);
    return result;
}

char *
test_read_all_from_file(FILE *file)
{
    char *buffer = calloc(8, sizeof(*buffer));
    size_t length = 0;
    size_t capacity = 8;
    while (!feof(file)) {
        size_t bytes_to_read = capacity - length;
        ssize_t bytes_read =
            fread(&buffer[length],
                  sizeof(*buffer),
                  bytes_to_read,
                  file);
        length += bytes_read;
        if (length == capacity) {
            capacity *= 2;
            buffer = realloc(buffer, capacity * sizeof(*buffer));
        }
    }

    return buffer;
}

int
test_result_report_default(test_result *result,
                           test_name_hierarchy const *hierarchy)
{
    return result->functions->report_default(result, hierarchy);
}

int
test_result_report_verbose(test_result *result,
                           test_name_hierarchy const *hierarchy)
{
    return result->functions->report_verbose(result, hierarchy);
}

int
test_result_report_json(test_result *result)
{
    return result->functions->report_json(result);
}

int
test_suite_result_report_default(test_suite_result *result,
                                 test_name_hierarchy const *parent)
{
    test_name_hierarchy hierarchy = {
        .parent       = parent,
        .name         = result->test_suite_->test.name,
    };
    size_t num_errors = 0;

    for (size_t i = 0; i < result->length; ++i) {
        test_result *test_result = result->results[i];
        num_errors += test_result_report_default(test_result, &hierarchy);
    }

    if (num_errors > 0) {
        test_name_hierarchy_print(&hierarchy);
        printf(": %ld error(s) in %f seconds.\n", num_errors,
               result->result.time);
    }

    return (int) num_errors;
}

int
test_suite_result_report_verbose(test_suite_result *result,
                                 test_name_hierarchy const *parent)
{
    test_name_hierarchy hierarchy = {
        .parent       = parent,
        .name         = result->test_suite_->test.name,
    };
    size_t num_errors = 0;

    for (size_t i = 0; i < result->length; ++i) {
        test_result *test_result = result->results[i];
        num_errors += test_result_report_verbose(test_result, &hierarchy);
    }

    test_name_hierarchy_print(&hierarchy);
    printf(": %ld error(s) in %f seconds.\n", num_errors,
           result->result.time);

    return (int) num_errors;
}

int
test_suite_result_report_json(test_suite_result *result)
{
    char const *suite_name = result->test_suite_->test.name;
    size_t num_errors = 0;

    printf("{\"type\": \"suite\", \"name\": \"%s\", \"time\": %f, "
           "\"members\": [",
           suite_name, result->result.time);
    for (size_t i = 0; i < result->length; ++i) {
        test_result *test_result = result->results[i];
        num_errors += test_result_report_json(test_result);
        if (i < result->length-1) {
            printf(", ");
        }
    }

    printf("], \"num_errors\": %li, \"pass\": ", num_errors);
    if (num_errors > 0) {
        printf("false");
    } else {
        printf("true");
    }
    printf("}");

    return (int) num_errors;
}

int
test_case_result_report_default(test_case_result *result,
                                test_name_hierarchy const *parent)
{
    test_name_hierarchy hierarchy = {
        .parent       = parent,
        .name         = result->test_case_->test.name,
    };
    const double time = result->result.time;

    if (!result->pass) {
        test_name_hierarchy_print(&hierarchy);
        printf(": error in %f seconds.\n", time);
        printf("%s", result->out_message);
        fprintf(stderr, "%s", result->err_message);
    }

    return !result->pass;
}

int
test_case_result_report_verbose(test_case_result *result,
                                test_name_hierarchy const *parent)
{
    test_name_hierarchy hierarchy = {
        .parent       = parent,
        .name         = result->test_case_->test.name,
    };
    const double time = result->result.time;

    test_name_hierarchy_print(&hierarchy);

    if (result->pass) {
        printf(": ok in %f seconds.\n", time);
        printf("%s", result->out_message);
    } else {
        printf(": error in %f seconds.\n", time);
        printf("%s", result->out_message);
        fprintf(stderr, "%s", result->err_message);
    }

    return !result->pass;
}

int
test_case_result_report_json(test_case_result *result)
{
    test_case *test_case_ = (test_case *) result->test_case_;
    const double time = result->result.time;

    char const *test_name = test_case_->test.name;
    char *escaped_output = test_string_escape(result->out_message);
    char *escaped_error = test_string_escape(result->err_message);

    printf("{\"type\": \"case\", \"name\": \"%s\", \"time\": %f, \"pass\": ",
           test_name, time);
    if (result->pass)
        printf("true");
    else
        printf("false");
    printf(", \"output\": \"%s\", \"error_message\": \"%s\"}", escaped_output,
           escaped_error);

    free(escaped_output);
    free(escaped_error);

    return !result->pass;
}

void
test_name_hierarchy_print(test_name_hierarchy const *hierarchy)
{
    if (hierarchy != NULL) {
        test_name_hierarchy_print(hierarchy->parent);
        if (hierarchy->parent != NULL)
            printf("/");
        printf("%s", hierarchy->name);
    }
}

double
test_time_function(test_thunk_func func, void *env, void **output)
{
    struct timespec start, end;
    clock_gettime(CLOCK_MONOTONIC, &start);
    void *temp = func(env);
    clock_gettime(CLOCK_MONOTONIC, &end);
    if (output != NULL)
        *output = temp;
    double secs = end.tv_sec - start.tv_sec;
    double nsecs = end.tv_nsec - start.tv_nsec;

    return secs + nsecs * 0.000000001;
}

char *
test_string_escape(char const *string)
{
    size_t length = 0;
    size_t capacity = strlen(string) + 2;
    char *output = calloc(capacity * sizeof(*output), 1);
    for (size_t i = 0; string[i] != '\0'; ++i) {
        switch(string[i]) {
        case '\b':
            output[length++] = '\\';    output[length++] = 'b';
            break;
        case '\f':
            output[length++] = '\\';    output[length++] = 'f';
            break;
        case '\n':
            output[length++] = '\\';    output[length++] = 'n';
            break;
        case '\r':
            output[length++] = '\\';    output[length++] = 'r';
            break;
        case '\t':
            output[length++] = '\\';    output[length++] = 't';
            break;
        case '\"':
            output[length++] = '\\';    output[length++] = '\"';
            break;
        case '\\':
            output[length++] = '\\';    output[length++] = '\\';
            break;
        default:
            output[length++] = string[i];
        }

        if (length + 2 == capacity) {
            capacity *= 2;
            output = realloc(output, capacity * sizeof(*output));
        }
    }

    output[length] = '\0';

    return output;
}

char *
test_construct_test_name(test_name_hierarchy const *hierarchy)
{
    if (hierarchy == NULL) {
        return calloc(sizeof(char), 1);
    }
    char *name = test_construct_test_name(hierarchy->parent);
    size_t name_length = strlen(name);
    char const *part_name = hierarchy->name;
    size_t part_length = strlen(part_name);
    name = realloc(name, name_length + part_length + 2);
    name[name_length] = '/';
    strcpy(&name[name_length+1], part_name);

    return name;
}

#endif

