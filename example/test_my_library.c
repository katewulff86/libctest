#include "ctest.h"
#include "my_library.h"

void my_function_test(void)
{
    assert(my_great_function() == 0);
}

typedef struct my_fixture_type {
    int wizz;
    char bang;
} my_fixture_type;

void my_function_with_fixture_test(my_fixture_type *object)
{
    assert(my_great_function() == object->wizz); /* oops! */
}

void *my_fixture_setup_function(void *o)
{
    (void) o;
    my_fixture_type *object = malloc(sizeof(*object));
    /* setup your fixture */
    object->wizz = 7;

    return object;
}

void my_fixture_teardown_function(my_fixture_type *object)
{
    /* teardown your fixture */
    free(object);
}

test_fixture my_fixture = {
    .parent = NULL,             /* fixtures can be nested */
    .setup = (test_setup_func) my_fixture_setup_function,
    .teardown = (test_teardown_func) my_fixture_teardown_function
};

TEST_CASE(my_function_test)
TEST_CASE_WITH_FIXTURE(my_function_with_fixture_test, &my_fixture)
TEST_SUITE(my_test_suite,
           &my_function_test_case,
           &my_function_with_fixture_test_case)
TEST_MAIN(&my_test_suite)
