#include <string.h>
#include "ctest.h"

static void
no_error_test()
{
}

static void
capture_output_aux_test()
{
    printf("Hello, World!\n");
}

TEST_CASE(capture_output_aux_test)

static void
capture_output_test()
{
    test_settings settings;
    settings.regex_length = 0;
    test_case_result *result = test_case_run(&capture_output_aux_test_case,
                                             NULL, &settings);

    assert(strcmp(result->out_message, "Hello, World!\n") == 0);
}

static void
capture_error_aux_test()
{
    fprintf(stderr, "Hello, World!\n");
}

TEST_CASE(capture_error_aux_test)

static void
capture_error_test()
{
    test_settings settings;
    settings.regex_length = 0;
    test_case_result *result = test_case_run(&capture_error_aux_test_case,
                                             NULL, &settings);
    assert(strcmp(result->err_message, "Hello, World!\n") == 0);
}

static void *
fixture_setup(void *e)
{
    (void) e;
    int *i = malloc(sizeof(*i));
    *i = 7;

    return i;
}

static void
fixture_teardown(void *e)
{
    free(e);
}

static void
fixture_test(int *i)
{
    assert(*i == 7);
}

test_fixture fixture_test_object = {
    .parent = NULL,
    .setup = fixture_setup,
    .teardown = fixture_teardown,
};

static void
excessive_text_test()
{
    for (size_t i = 0; i < 100000; ++i) {
        puts("excessive text to transfer from test to monitor");
    }
}

TEST_CASE(no_error_test)
TEST_CASE(capture_output_test)
TEST_CASE(capture_error_test)
TEST_CASE_WITH_FIXTURE(fixture_test, &fixture_test_object)
TEST_CASE(excessive_text_test)
TEST_SUITE(selftest_suite,
           &no_error_test_case,
           &capture_output_test_case,
           &capture_error_test_case,
           &fixture_test_case,
           &excessive_text_test_case)
TEST_MAIN(&selftest_suite)
